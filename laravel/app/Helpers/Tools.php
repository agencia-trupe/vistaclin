<?php

namespace App\Helpers;

class Tools
{
    public static function parseTelefones($telefones = null)
    {
        if (! $telefones) return;

        $telefonesArray = explode(',', $telefones);

        $parsed = '';
        foreach($telefonesArray as $telefone) {
            $telefone = preg_replace('/(.*)\s/', '<span>$1</span> ', trim($telefone));
            $parsed .= "<p>${telefone}</p>";
        }

        return $parsed;
    }
}
