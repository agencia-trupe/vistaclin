<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\ContatosRecebidosRequest;
use App\Http\Controllers\Controller;

use App\Models\Banner;
use App\Models\Institucional;
use App\Models\CorpoClinico;
use App\Models\Procedimento;
use App\Models\Exame;
use App\Models\Convenio;
use App\Models\Doenca;
use App\Models\Contato;
use App\Models\ContatoRecebido;

class HomeController extends Controller
{
    public function index()
    {
        return view('frontend.home')->with([
            'banners'       => Banner::ordenados()->get(),
            'institucional' => Institucional::first(),
            'corpoClinico'  => CorpoClinico::ordenados()->get(),
            'procedimentos' => Procedimento::ordenados()->get(),
            'exames'        => Exame::ordenados()->get(),
            'doencas'       => Doenca::ordenados()->get(),
            'convenios'     => Convenio::ordenados()->get(),
            'contato'       => Contato::first()
        ]);
    }

    public function doenca($id)
    {
        $doenca = Doenca::find($id);
        if (! $doenca) abort('404');

        return view('frontend.doenca', compact('doenca'));
    }

    public function contatoPost(ContatosRecebidosRequest $request)
    {
        ContatoRecebido::create($request->all());

        $contato = Contato::first();

        if (isset($contato->email)) {
            \Mail::send('emails.contato', $request->all(), function($message) use ($request, $contato)
            {
                $message->to($contato->email, config('site.name'))
                        ->subject('[CONTATO] '.config('site.name'))
                        ->replyTo($request->get('email'), $request->get('nome'));
            });
        }

        $response = [
            'status'  => 'success',
            'message' => 'Mensagem enviada com sucesso!'
        ];

        return response()->json($response);
    }
}
