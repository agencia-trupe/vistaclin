<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ProcedimentosRequest;
use App\Http\Controllers\Controller;

use App\Models\Procedimento;

class ProcedimentosController extends Controller
{
    public function index()
    {
        $registros = Procedimento::ordenados()->get();

        return view('painel.procedimentos.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.procedimentos.create');
    }

    public function store(ProcedimentosRequest $request)
    {
        try {

            $input = $request->all();

            Procedimento::create($input);
            return redirect()->route('painel.procedimentos.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Procedimento $registro)
    {
        return view('painel.procedimentos.edit', compact('registro'));
    }

    public function update(ProcedimentosRequest $request, Procedimento $registro)
    {
        try {

            $input = $request->all();

            $registro->update($input);
            return redirect()->route('painel.procedimentos.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Procedimento $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.procedimentos.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
