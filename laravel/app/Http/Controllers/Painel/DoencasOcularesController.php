<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\DoencasOcularesRequest;
use App\Http\Controllers\Controller;

use App\Models\Doenca;
use App\Helpers\CropImage;

class DoencasOcularesController extends Controller
{
    private $image_config = [
        'width'  => 255,
        'height' => 115,
        'path'   => 'assets/img/doencas-oculares/'
    ];

    public function index()
    {
        $registros = Doenca::ordenados()->get();

        return view('painel.doencas-oculares.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.doencas-oculares.create');
    }

    public function store(DoencasOcularesRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Doenca::create($input);
            return redirect()->route('painel.doencas-oculares.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Doenca $registro)
    {
        return view('painel.doencas-oculares.edit', compact('registro'));
    }

    public function update(DoencasOcularesRequest $request, Doenca $registro)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) {
                $input['imagem'] = CropImage::make('imagem', $this->image_config);
            }

            $registro->update($input);
            return redirect()->route('painel.doencas-oculares.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Doenca $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.doencas-oculares.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
