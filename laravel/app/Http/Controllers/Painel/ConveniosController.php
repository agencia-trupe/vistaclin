<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\ConveniosRequest;
use App\Http\Controllers\Controller;

use App\Models\Convenio;
use App\Helpers\CropImage;

class ConveniosController extends Controller
{
    private $image_config = [
        'width'  => 160,
        'height' => 160,
        'path'   => 'assets/img/convenios/'
    ];

    public function index()
    {
        $registros = Convenio::ordenados()->get();

        return view('painel.convenios.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.convenios.create');
    }

    public function store(ConveniosRequest $request)
    {
        try {

            $input = $request->all();
            $input['imagem'] = CropImage::make('imagem', $this->image_config);

            Convenio::create($input);
            return redirect()->route('painel.convenios.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(Convenio $registro)
    {
        return view('painel.convenios.edit', compact('registro'));
    }

    public function update(ConveniosRequest $request, Convenio $registro)
    {
        try {

            $input = array_filter($request->all(), 'strlen');
            if (isset($input['imagem'])) {
                $input['imagem'] = CropImage::make('imagem', $this->image_config);
            }

            $registro->update($input);
            return redirect()->route('painel.convenios.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(Convenio $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.convenios.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
