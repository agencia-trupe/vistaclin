<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\InstitucionalRequest;
use App\Http\Controllers\Controller;

use App\Models\Institucional;
use App\Helpers\CropImage;

class InstitucionalController extends Controller
{
    private $image_config = [
        [
            'width'  => 365,
            'height' => 245,
            'path'   => 'assets/img/institucional/thumbs/'
        ],
        [
            'width'  => 980,
            'height' => null,
            'upsize' => true,
            'path'   => 'assets/img/institucional/'
        ]
    ];

    public function index()
    {
        $registro = Institucional::first();

        return view('painel.institucional.edit', compact('registro'));
    }

    public function update(InstitucionalRequest $request, Institucional $registro)
    {
        try {

            $input = array_filter($request->all(), 'strlen');

            if (isset($input['imagem_1'])) {
                $input['imagem_1'] = CropImage::make('imagem_1', $this->image_config);
            }
            if (isset($input['imagem_2'])) {
                $input['imagem_2'] = CropImage::make('imagem_2', $this->image_config);
            }
            if (isset($input['imagem_3'])) {
                $input['imagem_3'] = CropImage::make('imagem_3', $this->image_config);
            }

            $registro->update($input);

            return redirect()->route('painel.institucional.index')->with('success', 'Registro alterado com sucesso.');
        } catch (\Exception $e) {
            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);
        }
    }
}
