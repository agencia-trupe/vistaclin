<?php

namespace App\Http\Controllers\Painel;

use Illuminate\Http\Request;

use App\Http\Requests\CorpoClinicoRequest;
use App\Http\Controllers\Controller;

use App\Models\CorpoClinico;

class CorpoClinicoController extends Controller
{
    public function index()
    {
        $registros = CorpoClinico::ordenados()->get();

        return view('painel.corpo-clinico.index', compact('registros'));
    }

    public function create()
    {
        return view('painel.corpo-clinico.create');
    }

    public function store(CorpoClinicoRequest $request)
    {
        try {

            $input = $request->all();

            CorpoClinico::create($input);
            return redirect()->route('painel.corpo-clinico.index')->with('success', 'Registro adicionado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao adicionar registro: '.$e->getMessage()]);

        }
    }

    public function edit(CorpoClinico $registro)
    {
        return view('painel.corpo-clinico.edit', compact('registro'));
    }

    public function update(CorpoClinicoRequest $request, CorpoClinico $registro)
    {
        try {

            $input = array_filter($request->all(), 'strlen');

            $registro->update($input);
            return redirect()->route('painel.corpo-clinico.index')->with('success', 'Registro alterado com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao alterar registro: '.$e->getMessage()]);

        }
    }

    public function destroy(CorpoClinico $registro)
    {
        try {

            $registro->delete();
            return redirect()->route('painel.corpo-clinico.index')->with('success', 'Registro excluído com sucesso.');

        } catch (\Exception $e) {

            return back()->withErrors(['Erro ao excluir registro: '.$e->getMessage()]);

        }
    }

}
