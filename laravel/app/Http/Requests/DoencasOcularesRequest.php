<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class DoencasOcularesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'imagem' => 'required|image',
            'descricao' => 'required',
            'texto' => 'required',
        ];

        if ($this->method() != 'POST') {
            $rules['imagem'] = 'image';
        }

        return $rules;
    }
}
