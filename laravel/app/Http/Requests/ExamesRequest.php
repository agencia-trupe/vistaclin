<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ExamesRequest extends Request
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        $rules = [
            'titulo' => 'required',
            'subtitulo' => '',
            'descricao' => 'required',
        ];

        if ($this->method() != 'POST') {
        }

        return $rules;
    }
}
