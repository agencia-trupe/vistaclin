<?php

Route::group(['middleware' => ['web']], function () {
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('doencas-oculares/{id}', 'HomeController@doenca')->name('doenca');
    Route::post('contato', 'HomeController@contatoPost')->name('contato.post');

    // Painel
    Route::group([
        'prefix'     => 'painel',
        'namespace'  => 'Painel',
        'middleware' => ['auth']
    ], function() {
        Route::get('/', 'PainelController@index')->name('painel');

        /* GENERATED ROUTES */
		Route::resource('doencas-oculares', 'DoencasOcularesController');
		Route::resource('convenios', 'ConveniosController');
        Route::resource('procedimentos', 'ProcedimentosController');
		Route::resource('exames', 'ExamesController');
		Route::resource('corpo-clinico', 'CorpoClinicoController');
		Route::resource('institucional', 'InstitucionalController', ['only' => ['index', 'update']]);
		Route::resource('banners', 'BannersController');
        Route::resource('contato/recebidos', 'ContatosRecebidosController');
        Route::resource('contato-info', 'ContatoController');
        Route::resource('usuarios', 'UsuariosController');

        Route::post('order', 'PainelController@order');
        Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index');
    });

    // Auth
    Route::group([
        'prefix'    => 'painel',
        'namespace' => 'Auth'
    ], function() {
        Route::get('login', 'AuthController@showLoginForm')->name('auth');
        Route::post('login', 'AuthController@login')->name('login');
        Route::get('logout', 'AuthController@logout')->name('logout');
    });
});
