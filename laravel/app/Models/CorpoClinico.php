<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CorpoClinico extends Model
{
    protected $table = 'corpo_clinico';

    protected $guarded = ['id'];

    public function scopeOrdenados($query)
    {
        return $query->orderBy('ordem', 'ASC')->orderBy('id', 'DESC');
    }
}
