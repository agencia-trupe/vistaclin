<?php

namespace App\Console\Commands;

trait ResourceTrait {
    public function confirmOverwrite($file)
    {
        if ( ! file_exists($file) || $this->confirm('<comment>' . basename($file) . '</comment> already exists! Overwrite this file? [y|N]')) {
            return true;
        }

        return false;
    }

    public function createPhpFile($file, $content)
    {
        file_put_contents($file, '<?php' . "\n\n" . $content);
    }

    public function createViewFile($file, $content)
    {
        file_put_contents($file, $content);
    }

    public function routeName()
    {
        return str_replace('_', '-', $this->table);
    }

    public function tableTitleCase()
    {
        $string = mb_convert_case(str_replace('_', ' ', $this->table), MB_CASE_TITLE, 'UTF-8');
        $string = str_replace(' ', '', $string);

        return $string;
    }

    public function routesCommentHandle()
    {
        return '/* GENERATED ROUTES */';
    }
}