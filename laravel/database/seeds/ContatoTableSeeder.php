<?php

use Illuminate\Database\Seeder;

class ContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('contato')->insert([
            'email'       => 'contato@vistaclin.com.br',
            'telefones'   => '11 4184·3234, 11 4183·6961',
            'endereco'    => '<p>Av. Rui Barbosa, 1009 · cj 01</p><p>06300-000 · Centro · Carapicuíba, SP</p>',
            'google_maps' => '<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3658.3429835951524!2d-46.84202308451239!3d-23.520163284703457!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cf01ebdf612b07%3A0xb2d88e67c1405f7d!2sAv.+Rui+Barbosa%2C+1009+-+Centro%2C+Carapicu%C3%ADba+-+SP!5e0!3m2!1spt-BR!2sbr!4v1465401986776" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>',
        ]);
    }
}
