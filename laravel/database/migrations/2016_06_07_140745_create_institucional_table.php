<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateInstitucionalTable extends Migration
{
    public function up()
    {
        Schema::create('institucional', function (Blueprint $table) {
            $table->increments('id');
            $table->string('imagem_1');
            $table->string('imagem_2');
            $table->string('imagem_3');
            $table->text('texto');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop('institucional');
    }
}
