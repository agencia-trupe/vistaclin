(function(window, document, $, undefined) {
    'use strict';

    var App = {};

    App.shrinkHeader = function() {
        $(window).on('scroll', function(event) {
            var distance  = $(document).scrollTop(),
                threshold = $('#institucional').offset().top - 120,
                header    = $('header');

            if (distance > threshold && window.innerWidth > 1199) {
                header.addClass('shrink');
            } else {
                header.removeClass('shrink');
            }
        });

        $(window).trigger('scroll');
    };

    App.mobileToggle = function() {
        var $handle = $('#mobile-handle'),
            $nav    = $('#nav-mobile');

        $handle.on('click touchstart', function(event) {
            event.preventDefault();
            $nav.slideToggle();
            $handle.toggleClass('close');
        });
    };

    App.navScroll = function() {
        $('.nav-scroll').on('click touchstart', function(event) {
            event.preventDefault();

            var section = $(this).attr('href');
            if (section === '/') section = 'top';

            $('html, body').animate({
                scrollTop: $('#' + section).offset().top - (window.innerWidth > 1199 ? 69 : 0)
            });

            if ($(this).parent().is('#nav-mobile')) {
                $('#nav-mobile').slideToggle();
                $('#mobile-handle').toggleClass('close');
            }
        });
    };

    App.bannersHome = function() {
        $('.banners-wrapper').cycle({
            slides: '>.banner',
        });
    };

    App.saibaMais = function() {
        var $handle = $('.saiba-mais');

        $handle.click(function(event) {
            event.preventDefault();

            $handle.toggleClass('open');
            $handle.next().slideToggle();
        });
    };

    App.contentLightbox = function() {
        $('.lightbox-content').fancybox({
            helpers: {
                overlay: {
                    locked: false,
                    css: {'background-color': 'rgba(0,0,0,.5)'},
                }
            },
            type: 'ajax',
            maxWidth: 980,
            maxHeight: '95%',
            padding: 0
        });
    };

    App.imageLightbox = function() {
        $('.lightbox-image').fancybox({
            helpers: {
                overlay: {
                    locked: false,
                    css: {'background-color': 'rgba(0,0,0,.5)'},
                },
                title: {
                    type: 'inside'
                }
            },
            maxHeight: '85%',
            padding: 10
        });
    };

    App.envioContato = function(event) {
        event.preventDefault();

        var $form     = $(this),
            $response = $('#form-contato-response');

        $response.fadeOut('fast');

        $.ajax({
            type: "POST",
            url: $('base').attr('href') + '/contato',
            data: {
                nome: $('#nome').val(),
                email: $('#email').val(),
                telefone: $('#telefone').val(),
                mensagem: $('#mensagem').val(),
            },
            success: function(data) {
                $response.fadeOut().text(data.message).fadeIn('slow');
                $form[0].reset();
            },
            error: function(data) {
                if (data.responseJSON) {
                    var error = data.responseJSON[Object.keys(data.responseJSON)[0]];
                    $response.fadeOut().text(error).fadeIn('slow');
                }
            },
            dataType: 'json'
        });
    };

    App.init = function() {
        this.shrinkHeader();
        this.mobileToggle();
        this.navScroll();
        this.saibaMais();
        this.contentLightbox();
        this.imageLightbox();
        this.bannersHome();
        $('#form-contato').on('submit', this.envioContato);
    };

    $(document).ready(function() {
        App.init();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    });

}(window, document, jQuery));
