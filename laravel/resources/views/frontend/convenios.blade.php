<section id="convenios">
    <div class="center">
        <h2>CONVÊNIOS</h2>
        <h3>VEJA TODOS OS CONVÊNIOS ATENDIDOS</h3>

        <div class="convenios-thumbs">
            @foreach($convenios as $convenio)
                <div>
                    <img src="{{ asset('assets/img/convenios/'.$convenio->imagem) }}" alt="{{ $convenio->nome }}">
                </div>
            @endforeach
        </div>
    </div>
</section>
