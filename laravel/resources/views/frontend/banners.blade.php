<section id="banners">
    <div class="banners-wrapper">
        @foreach($banners as $banner)
        <div class="banner" style="background-image: url('{{ asset('assets/img/banners/'.$banner->imagem) }}')">
            <div class="center">
                <div class="banner-content-wrapper">
                    <div class="banner-content">
                        <div class="banner-content-box">
                            <h1>{{ $banner->titulo }}</h1>
                            <p>{{ $banner->texto }}</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endforeach
    </div>
</section>
