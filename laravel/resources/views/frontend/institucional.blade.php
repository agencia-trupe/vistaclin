<section id="institucional">
    <div class="institucional-imagens">
        <div class="center">
            <h2>CONHEÇA NOSSA CLÍNICA</h2>
            <h3>LOCALIZADA NO CENTRO DE CARAPICUÍBA, DISPÕE DE MÉDICOS EXCELENTES E EM CONSTANTE ATUALIZAÇÃO, COM TÍTULO DE ESPECIALISTA PELO CONSELHO BRASILEIRO DE OFTALMOLOGIA</h3>

            <div class="imagens">
                @for($i = 1; $i <= 3; $i++)
                <a href="{{ asset('assets/img/institucional/'.$institucional->{'imagem_'.$i}) }}" class="lightbox-image" rel="institucional">
                    <img src="{{ asset('assets/img/institucional/thumbs/'.$institucional->{'imagem_'.$i}) }}" alt="">
                </a>
                @endfor
            </div>
        </div>
    </div>

    <div class="institucional-texto">
        <div class="center">
            <a href="#" class="saiba-mais">SAIBA MAIS</a>
            <div class="hidden">
                <div class="texto">{!! $institucional->texto !!}</div>

                <h2>CORPO CLÍNICO</h2>
                <div class="corpo-clinico">
                    @foreach($corpoClinico as $medico)
                    <div class="medico">
                        <p class="titulo">
                            {{ $medico->nome }}<br>
                            CRM {{ $medico->crm }}
                        </p>
                        {!! $medico->texto !!}
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</section>
