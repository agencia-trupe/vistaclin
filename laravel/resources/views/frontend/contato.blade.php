<section id="contato">
    <div class="center">
        <h2>LOCALIZAÇÃO &middot; CONTATO</h2>

        <div class="contato-info">
            <div class="telefones">
                {!! Tools::parseTelefones($contato->telefones) !!}
            </div>
            <div class="endereco">{!! $contato->endereco !!}</div>
            <div class="googlemaps">{!! $contato->google_maps !!}</div>
        </div>

        <div class="contato-form">
            <form action="" id="form-contato" method="POST">
                <input type="text" name="nome" id="nome" placeholder="nome" required>
                <input type="email" name="email" id="email" placeholder="e-mail" required>
                <input type="text" name="telefone" id="telefone" placeholder="telefone">
                <textarea name="mensagem" id="mensagem" placeholder="mensagem" required></textarea>
                <input type="submit" value="enviar">
                <div id="form-contato-response"></div>
            </form>
        </div>
    </div>
</section>
