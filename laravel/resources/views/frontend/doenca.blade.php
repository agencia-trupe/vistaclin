<div class="doenca-lightbox">
    <div class="doenca-titulo">
        <img src="{{ asset('assets/img/doencas-oculares/'.$doenca->imagem) }}" alt="">
        <h1>{{ $doenca->titulo }}</h1>
    </div>

    <div class="doenca-texto">
        {!! $doenca->texto !!}
    </div>
</div>
