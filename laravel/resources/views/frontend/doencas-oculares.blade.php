<section id="doencas-oculares">
    <div class="center">
        <h2>PRINCIPAIS DOENÇAS OCULARES</h2>
        <h3>VEJA AQUI AS PRINCIPAIS DOENÇAS OCULARES</h3>

        <div class="doencas-oculares-thumbs">
            @foreach($doencas as $doenca)
            <a href="{{ route('doenca', $doenca->id) }}" class="lightbox-content">
                <img src="{{ asset('assets/img/doencas-oculares/'.$doenca->imagem) }}" alt="">
                <div class="texto">
                    <h4>{{ $doenca->titulo }}</h4>
                    <p>{{ $doenca->descricao }}</p>
                </div>
            </a>
            @endforeach
        </div>
    </div>
</section>
