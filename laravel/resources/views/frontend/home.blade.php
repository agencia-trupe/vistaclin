@extends('frontend.common.template')

@section('content')

    @include('frontend.banners')
    @include('frontend.institucional')
    @include('frontend.procedimentos')
    @include('frontend.convenios')
    @include('frontend.doencas-oculares')
    @include('frontend.contato')

@endsection
