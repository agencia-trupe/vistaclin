<section id="procedimentos">
    <div class="center">
        <h2>PROCEDIMENTOS</h2>
        <h3>CONHEÇA OS PROCEDIMENTOS QUE REALIZAMOS</h3>

        <div class="procedimentos-list">
            @foreach($procedimentos as $procedimento)
            <div>
                <h4>{{ $procedimento->titulo }}</h4>
                @if($procedimento->subtitulo)
                <h5>{{ $procedimento->subtitulo }}</h5>
                @endif
                <p>{{ $procedimento->descricao }}</p>
            </div>
            @endforeach
        </div>

        <h2 class="exames">EXAMES</h2>
        <h3>CONHEÇA OS EXAMES QUE REALIZAMOS</h3>

        <div class="procedimentos-list">
            @foreach($exames as $procedimento)
            <div>
                <h4>{{ $procedimento->titulo }}</h4>
                @if($procedimento->subtitulo)
                <h5>{{ $procedimento->subtitulo }}</h5>
                @endif
                <p>{{ $procedimento->descricao }}</p>
            </div>
            @endforeach
        </div>
    </div>
</section>
