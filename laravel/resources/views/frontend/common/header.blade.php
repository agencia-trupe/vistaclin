    <header>
        <div class="center">
            <a href="/" class="header-logo nav-scroll">
                {{ config('site.name') }}
            </a>

            <div class="nav-wrapper">
                <nav id="nav-desktop">
                    @include('frontend.common._nav')
                    <div class="telefones">
                        {!! Tools::parseTelefones($contato->telefones) !!}
                    </div>
                </nav>
            </div>

            <a href="#" id="mobile-handle">MENU</a>
        </div>

        <nav id="nav-mobile">
            @include('frontend.common._nav')
        </nav>
    </header>
