    <footer>
        <div class="center">
            <p>
                © {{ date('Y') }} {{ config('site.name') }} &middot; Todos os direitos reservados
                <span></span>
                <a href="http://trupe.net" target="_blank">Criação de sites</a>:
                <a href="http://trupe.net" target="_blank">Trupe Agência Criativa</a>
            </p>
        </div>
    </footer>
