<ul class="nav navbar-nav">
    <li @if(str_is('painel.banners*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.banners.index') }}">Banners</a>
    </li>
    <li @if(str_is('painel.institucional*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.institucional.index') }}">Institucional</a>
    </li>
    <li @if(str_is('painel.corpo-clinico*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.corpo-clinico.index') }}">Corpo Clínico</a>
    </li>
    <li @if(str_is('painel.procedimentos*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.procedimentos.index') }}">Procedimentos</a>
    </li>
    <li @if(str_is('painel.exames*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.exames.index') }}">Exames</a>
    </li>
    <li @if(str_is('painel.convenios*', Route::currentRouteName())) class="active" @endif>
        <a href="{{ route('painel.convenios.index') }}">Convênios</a>
    </li>
	<li @if(str_is('painel.doencas-oculares*', Route::currentRouteName())) class="active" @endif>
		<a href="{{ route('painel.doencas-oculares.index') }}">Doenças Oculares</a>
	</li>
    <li class="dropdown @if(str_is('painel.contato*', Route::currentRouteName())) active @endif">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
            Contato
            @if($contatosNaoLidos >= 1)
            <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
            @endif
            <b class="caret"></b>
        </a>
        <ul class="dropdown-menu">
            <li><a href="{{ route('painel.contato-info.index') }}">Informações de Contato</a></li>
            <li><a href="{{ route('painel.contato.recebidos.index') }}">
                Contatos Recebidos
                @if($contatosNaoLidos >= 1)
                <span class="label label-success" style="margin-left:3px;">{{ $contatosNaoLidos }}</span>
                @endif
            </a></li>
        </ul>
    </li>
</ul>
