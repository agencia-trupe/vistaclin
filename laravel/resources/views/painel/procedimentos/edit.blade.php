@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Procedimentos /</small> Editar Procedimento</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.procedimentos.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.procedimentos.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
