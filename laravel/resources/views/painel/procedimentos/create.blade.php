@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Procedimentos /</small> Adicionar Procedimento</h2>
    </legend>

    {!! Form::open(['route' => 'painel.procedimentos.store', 'files' => true]) !!}

        @include('painel.procedimentos.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
