@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Corpo Clínico /</small> Editar Corpo Clínico</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.corpo-clinico.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.corpo-clinico.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
