@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Corpo Clínico /</small> Adicionar Corpo Clínico</h2>
    </legend>

    {!! Form::open(['route' => 'painel.corpo-clinico.store', 'files' => true]) !!}

        @include('painel.corpo-clinico.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
