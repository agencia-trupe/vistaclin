@include('painel.common.flash')

<div class="row">
    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_1', 'Imagem 1') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/institucional/thumbs/'.$registro->imagem_1) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width: 400px;">
        @endif
            {!! Form::file('imagem_1', ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_2', 'Imagem 2') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/institucional/thumbs/'.$registro->imagem_2) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width: 400px;">
        @endif
            {!! Form::file('imagem_2', ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="col-md-4">
        <div class="well form-group">
            {!! Form::label('imagem_3', 'Imagem 3') !!}
        @if($submitText == 'Alterar')
            <img src="{{ url('assets/img/institucional/thumbs/'.$registro->imagem_3) }}" style="display:block; margin-bottom: 10px; width: 100%; max-width: 400px;">
        @endif
            {!! Form::file('imagem_3', ['class' => 'form-control']) !!}
        </div>
    </div>
</div>

<div class="form-group">
    {!! Form::label('texto', 'Texto') !!}
    {!! Form::textarea('texto', null, ['class' => 'form-control ckeditor', 'data-editor' => 'padrao']) !!}
</div>

{!! Form::submit($submitText, ['class' => 'btn btn-success']) !!}
