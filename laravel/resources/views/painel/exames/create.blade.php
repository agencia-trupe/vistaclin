@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Exames /</small> Adicionar Exame</h2>
    </legend>

    {!! Form::open(['route' => 'painel.exames.store', 'files' => true]) !!}

        @include('painel.exames.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
