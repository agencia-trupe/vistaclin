@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Exames /</small> Editar Exame</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.exames.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.exames.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
