@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Convênios /</small> Adicionar Convênio</h2>
    </legend>

    {!! Form::open(['route' => 'painel.convenios.store', 'files' => true]) !!}

        @include('painel.convenios.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
