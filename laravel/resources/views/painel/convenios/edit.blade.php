@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Convênios /</small> Editar Convênio</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.convenios.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.convenios.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
