@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Doenças Oculares /</small> Editar Doença</h2>
    </legend>

    {!! Form::model($registro, [
        'route'  => ['painel.doencas-oculares.update', $registro->id],
        'method' => 'patch',
        'files'  => true])
    !!}

    @include('painel.doencas-oculares.form', ['submitText' => 'Alterar'])

    {!! Form::close() !!}

@endsection
