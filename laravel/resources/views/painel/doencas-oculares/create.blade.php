@extends('painel.common.template')

@section('content')

    <legend>
        <h2><small>Doenças Oculares /</small> Adicionar Doença</h2>
    </legend>

    {!! Form::open(['route' => 'painel.doencas-oculares.store', 'files' => true]) !!}

        @include('painel.doencas-oculares.form', ['submitText' => 'Inserir'])

    {!! Form::close() !!}

@endsection
