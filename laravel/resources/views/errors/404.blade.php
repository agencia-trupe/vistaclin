@extends('frontend.common.template', [
    'erro404' => true
])

@section('content')

    <section id="not-found">
        <div class="wrapper">
            <img src="{{ asset('assets/img/layout/vistaclin.png') }}" alt="">
            <h3>PÁGINA NÃO ENCONTRADA</h3>
            <a href="{{ route('home') }}">VOLTAR PARA A HOME</a>
        </div>
    </section>

@endsection
