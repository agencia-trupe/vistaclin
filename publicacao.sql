-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: vistaclin_pre.mysql.dbaas.com.br    Database: vistaclin_pre
-- ------------------------------------------------------
-- Server version	5.6.30-76.3-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `banners`
--

DROP TABLE IF EXISTS `banners`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `banners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `banners`
--

LOCK TABLES `banners` WRITE;
/*!40000 ALTER TABLE `banners` DISABLE KEYS */;
INSERT INTO `banners` VALUES (1,1,'9_20160627195442.jpg','CLÍNICA DE OLHOS','Fazemos atendimento particular e de convênios, adultos e crianças, atendimentos de urgências e cirurgias.','2016-06-14 16:57:57','2016-06-27 22:54:43'),(2,2,'222_20160615165823.jpg','EQUIPAMENTOS MODERNOS','Estrutura moderna e constante atualização da equipe. Realizamos exames necessários ao diagnóstico de doenças oculares.','2016-06-14 16:58:21','2016-06-20 20:44:36');
/*!40000 ALTER TABLE `banners` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contato`
--

DROP TABLE IF EXISTS `contato`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contato` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefones` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `endereco` text COLLATE utf8_unicode_ci NOT NULL,
  `google_maps` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contato`
--

LOCK TABLES `contato` WRITE;
/*!40000 ALTER TABLE `contato` DISABLE KEYS */;
INSERT INTO `contato` VALUES (1,'contato@vistaclin.com.br','11 4184·3234, 11 4183·6961','<p>Av. Rui Barbosa, 1009 · cj 01</p><p>06300-000 · Centro · Carapicuíba, SP</p>','<iframe src=\"https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3658.3429835951524!2d-46.84202308451239!3d-23.520163284703457!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x94cf01ebdf612b07%3A0xb2d88e67c1405f7d!2sAv.+Rui+Barbosa%2C+1009+-+Centro%2C+Carapicu%C3%ADba+-+SP!5e0!3m2!1spt-BR!2sbr!4v1465401986776\" width=\"600\" height=\"450\" frameborder=\"0\" style=\"border:0\" allowfullscreen></iframe>',NULL,NULL);
/*!40000 ALTER TABLE `contato` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contatos_recebidos`
--

DROP TABLE IF EXISTS `contatos_recebidos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contatos_recebidos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `telefone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `mensagem` text COLLATE utf8_unicode_ci NOT NULL,
  `lido` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contatos_recebidos`
--

LOCK TABLES `contatos_recebidos` WRITE;
/*!40000 ALTER TABLE `contatos_recebidos` DISABLE KEYS */;
/*!40000 ALTER TABLE `contatos_recebidos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `convenios`
--

DROP TABLE IF EXISTS `convenios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `convenios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `convenios`
--

LOCK TABLES `convenios` WRITE;
/*!40000 ALTER TABLE `convenios` DISABLE KEYS */;
INSERT INTO `convenios` VALUES (1,13,'Mediservice','8_20160615140618.jpg','2016-06-14 17:04:42','2016-06-20 20:56:57'),(2,11,'Lincx','7_20160615140525.jpg','2016-06-14 17:04:58','2016-06-20 20:56:44'),(3,16,'Postal Saude','5_20160615140404.jpg','2016-06-14 17:05:00','2016-06-20 20:56:31'),(4,5,'Bradesco','4_20160615140315.jpg','2016-06-14 17:05:03','2016-06-20 20:59:01'),(5,8,'Cassi','3_20160615141801.jpg','2016-06-14 17:05:05','2016-06-15 17:18:01'),(6,3,'Amil','convenio_20160614140507.gif','2016-06-14 17:05:07','2016-06-14 17:05:07'),(7,7,'Caixa','2_20160615135854.jpg','2016-06-14 17:05:09','2016-06-15 16:58:54'),(8,12,'Maritima','1_20160615135705.jpg','2016-06-14 17:05:11','2016-06-15 16:57:05'),(9,14,'One','9_20160615140704.jpg','2016-06-15 17:07:04','2016-06-20 20:59:26'),(10,9,'Dix','10_20160615140803.jpg','2016-06-15 17:08:03','2016-06-15 17:08:03'),(11,1,'Allianz','11_20160615140909.jpg','2016-06-15 17:09:09','2016-06-20 20:57:12'),(12,10,'Geap','12_20160615141006.jpg','2016-06-15 17:10:06','2016-06-20 20:58:33'),(13,15,'OAB','13_20160615141130.jpg','2016-06-15 17:11:30','2016-06-15 17:11:30'),(14,4,'Apeoesp','14_20160615141257.jpg','2016-06-15 17:12:57','2016-06-20 20:58:52'),(15,2,'Ameplan','15_20160615141417.jpg','2016-06-15 17:14:18','2016-06-15 17:14:18'),(16,6,'Cabesp','16_20160615141511.jpg','2016-06-15 17:15:11','2016-06-15 17:15:11');
/*!40000 ALTER TABLE `convenios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `corpo_clinico`
--

DROP TABLE IF EXISTS `corpo_clinico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `corpo_clinico` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `nome` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `crm` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `corpo_clinico`
--

LOCK TABLES `corpo_clinico` WRITE;
/*!40000 ALTER TABLE `corpo_clinico` DISABLE KEYS */;
INSERT INTO `corpo_clinico` VALUES (1,1,'medico1_20160614140044.png','DRA. SILVIA M. TAMAKI','85 983','Formada pela Faculdade de Medicina da USP. \r\nEspecialização em Oftalmologia pela Santa Casa de SP.                                                     \r\nTitulo de especialista pelo Conselho Brasileiro de Oftalmologia','2016-06-14 17:00:44','2016-06-28 03:05:51'),(2,2,'medico2_20160614140122.jpg','DR. EDSON T. KIKUTI','85 860','Formado pela Santa Casa de SP. \r\nEspecialização em Oftalmologia pela Santa Casa de SP.\r\nTitulo de especialista pelo Conselho Brasileiro de Oftalmologia','2016-06-14 17:01:22','2016-06-21 03:06:23');
/*!40000 ALTER TABLE `corpo_clinico` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `doencas_oculares`
--

DROP TABLE IF EXISTS `doencas_oculares`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `doencas_oculares` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `doencas_oculares`
--

LOCK TABLES `doencas_oculares` WRITE;
/*!40000 ALTER TABLE `doencas_oculares` DISABLE KEYS */;
INSERT INTO `doencas_oculares` VALUES (1,2,'CATARATA','olho-com-catarata_20160615155343.jpg','Catarata consiste na opacidade total ou parcial do cristalino, lente natural do globo ocular, que é responsável pela focalização da visão para perto e para longe.','<p>A catarata atinge quase metade (46,2%) da popula&ccedil;&atilde;o mundial com mais de 65 anos. Estima-se que no mundo cerca de 160 milh&otilde;es de pessoas tenham esta doen&ccedil;a, considerada a maior causa de cegueira evit&aacute;vel. No Brasil s&atilde;o 2 milh&otilde;es e surgem cerca de 120 mil novos casos ao ano.</p>\r\n\r\n<p><strong>Sintomas da catarata</strong></p>\r\n\r\n<p>Os sintomas mais frequentes da catarata s&atilde;o: diminui&ccedil;&atilde;o da acuidade visual, sensa&ccedil;&atilde;o de vis&atilde;o &ldquo;nublada ou enevoada&rdquo;, sensibilidade maior &agrave; luz, altera&ccedil;&atilde;o da vis&atilde;o de cores e mudan&ccedil;a frequente do grau dos &oacute;culos.</p>\r\n','2016-06-14 17:06:39','2016-06-28 14:03:16'),(2,11,'MIOPIA','miopia_20160614140741.jpg','Condição em que os objetos próximos são vistos claramente, mas os objetos distantes não.','<p>Miopia &eacute; o dist&uacute;rbio visual que acarreta uma focaliza&ccedil;&atilde;o da imagem antes desta chegar &agrave; retina. Uma pessoa m&iacute;ope consegue ver objetos pr&oacute;ximos com nitidez, mas os distantes s&atilde;o visualizados como se estivessem emba&ccedil;ados (desfocados).</p>\r\n','2016-06-14 17:07:41','2016-06-15 18:47:50'),(3,10,'HIPERMETROPIA','hipermetropia_20160614140826.jpg','A hipermetropia ocorre quando o olho é um pouco menor do que o normal, provocando uma focalização errada da imagem, que se forma após a retina. ','<p>Ela tamb&eacute;m pode ser causada pela diminui&ccedil;&atilde;o do poder refrativo do olho, causada por altera&ccedil;&otilde;es no formato na&nbsp;c&oacute;rnea&nbsp;ou no cristalino.&nbsp;<br />\r\n<br />\r\nGeralmente o paciente com&nbsp;hipermetropia&nbsp;tem boa vis&atilde;o de longe, pois se seu grau n&atilde;o for muito elevado &eacute; naturalmente corrigido pelo aumento do poder di&oacute;ptrico do cristalino, em um processo chamado de acomoda&ccedil;&atilde;o. A maior parte das crian&ccedil;as apresenta&nbsp;hipermetropia, porque seus olhos normalmente s&atilde;o menores do que deveriam ser, por&eacute;m elas t&ecirc;m um maior poder de acomoda&ccedil;&atilde;o e suportam graus muito mais elevados. S&atilde;o comuns casos de pessoas que necessitam de &oacute;culos na inf&acirc;ncia, mas deixam de us&aacute;-los na idade adulta, quando o olho atinge o tamanho ideal.&nbsp;<br />\r\n<br />\r\nA&nbsp;hipermetropia&nbsp;tamb&eacute;m pode estar associada ao aparecimento de estrabismo acomodativo na inf&acirc;ncia, com o surgimento de sintomas ao redor dos 2 anos de idade. Neste caso a corre&ccedil;&atilde;o total do problema pode ser feita com o uso de lentes de &oacute;culos adequadas.&nbsp;</p>\r\n','2016-06-14 17:08:26','2016-06-15 18:48:45'),(4,1,'ASTIGMATISMO','astigmatismo_20160614140927.jpg','O astigmatismo pode resultar em visão borrada ou distorcida em todas as distâncias, dependendo do grau do astigmatismo. Pode deixar os olhos mais sensíveis à luz também.','<p>Pessoas com astigmatismo s&atilde;o muitas vezes&nbsp;m&iacute;opes&nbsp;ou&nbsp;hipermetropes&nbsp;tamb&eacute;m.</p>\r\n\r\n<p>Lembre-se de consultar sempre um oftalmologista, pois ele &eacute; o profissional respons&aacute;vel por diagnosticar e tratar qualquer uma dessas condi&ccedil;&otilde;es oculares.</p>\r\n\r\n<p><strong>O que causa o astigmatismo?</strong></p>\r\n\r\n<p>As imagens que seus olhos transmitem para seu c&eacute;rebro s&atilde;o n&iacute;tidas apenas se os raios de luz que passam para dentro dos olhos se concentram em um &uacute;nico ponto de sua retina, na parte de tr&aacute;s do seu olho. &nbsp;</p>\r\n\r\n<p>O astigmatismo ocorre quando a superf&iacute;cie do olho (a c&oacute;rnea) ou o cristalino atr&aacute;s dele, tem uma forma irregular. Ao inv&eacute;s de ser redondo, ele tem uma forma mais semelhante a uma bola de r&uacute;gbi. Como resultado, a luz n&atilde;o se concentra corretamente na retina e a imagem &eacute; emba&ccedil;ada.&nbsp;</p>\r\n','2016-06-14 17:09:28','2016-06-23 17:55:25'),(5,4,'CERATITE','images_20160615155513.jpg','Trata-de de uma alteração da córnea que pode ser um problema agudo ou crônico e levar ao deficit visual.','<p>S&atilde;o v&aacute;rios os tipos de ceratites, que podem estar relacionadas ao uso de&nbsp;lentes de contato, olho&nbsp;seco,&nbsp;doen&ccedil;as sist&ecirc;micas, traumatismos oculares&nbsp;e uso de determinados medicamentos.</p>\r\n\r\n<p>Os sintomas incluem dor, lacrimejamento, fotofobia, diminui&ccedil;&atilde;o de vis&atilde;o e ard&ecirc;ncia.</p>\r\n\r\n<p>O exame oftalmol&oacute;gico completo poder&aacute; determinar a causa e indicar o melhor tratamento.</p>\r\n','2016-06-15 18:55:13','2016-06-28 14:04:01'),(7,5,'CERATOCONE','ceratocone_20160615155618.jpg','Ceratocone é uma desordem ocular não-inflamatória e  caracterizada pelo afinamento progressivo e aumento da curvatura da parte central da córnea. ','<p>No ceratocone, a c&oacute;rnea assume uma forma de cone, por isso, o nome, o que acarreta na percep&ccedil;&atilde;o de imagens distorcidas. O principal sintoma dessa doen&ccedil;a &eacute; a diminui&ccedil;&atilde;o da vis&atilde;o.</p>\r\n\r\n<p>&Eacute; ainda muito mais frequente em portadores de s&iacute;ndromes como a de Down, Turner, Ehlers-Danlos e de Marfan, al&eacute;m de pessoas al&eacute;rgicas, entre outros casos. Muitos pacientes n&atilde;o percebem que t&ecirc;m o problema porque inicia-se com miopia e astigmatismo no olho. Isso pode evoluir rapidamente ou em outros casos levar anos para desenvolver-se. Pode ainda afetar gravemente e limitar as pessoas diante de tarefas do dia-a-dia.</p>\r\n','2016-06-15 18:56:18','2016-06-21 15:55:03'),(8,8,'ESTRABISMO','estr_20160628111122.jpg','Estrabismo é um tipo de alteração ocular que desalinha os olhos para direções diferentes e representa a perda do paralelismo dos olhos. ','<p>O desvio dos olhos pode ser constante e sempre notado, ou poder&aacute; ter per&iacute;odos normais e per&iacute;odos com olhos desviados. Um dos olhos poder&aacute; estar direcionado para frente, enquanto o outro desvia para dentro, para fora, para cima ou para baixo.</p>\r\n','2016-06-15 18:57:54','2016-06-28 14:11:22'),(9,9,'GLAUCOMA','glaucoma1_20160615155850.jpg','O glaucoma pode lesar o nervo óptico, ocasionando diminuição da visão e do campo visual. Importante realizar-se a prevenção, através da medida da pressão intraocular e do exame oftalmológico completo.','<p>O diagn&oacute;stico precoce pode preservar a vis&atilde;o do olho glaucomatoso e torna-se determinante um exame oftalmol&oacute;gico anual para todas as pessoas. Quando n&atilde;o h&aacute; dor, o paciente com glaucoma, muitas vezes, nem percebe que est&aacute; perdendo gradativamente e pode perder a vis&atilde;o nos est&aacute;gios finais da doen&ccedil;a. Com isso, a vis&atilde;o encontra-se prejudicada e o dano, em geral, torna-se irrevers&iacute;vel.</p>\r\n','2016-06-15 18:58:50','2016-06-21 16:02:20'),(10,14,'TERÇOL/CALÁZIO','tercol_20160615160054.jpg','É a infecção de uma pequena glândula da pálpebra, podendo ser interno ou externo conforme a glândula atingida.','<p>Inicia-se como um pequeno incha&ccedil;o na p&aacute;lpebra, o qual apresenta vermelhid&atilde;o&nbsp;e pode ficar um pouco dolorido. Geralmente o ter&ccedil;ol inflama e&nbsp;acumula pus, que acaba extravazando.</p>\r\n\r\n<p>Quando atinge as gl&acirc;ndulas de meibomius, pode&nbsp;se tornar cr&ocirc;nico,&nbsp;deixando&nbsp;um n&oacute;dulo indolor na p&aacute;lpebra chamado cal&aacute;zio.</p>\r\n','2016-06-15 19:00:18','2016-06-28 14:08:02'),(11,3,'CATARATA CONGÊNITA','catarata-img4_20160615160212.png','A catarata congênita ocorre por alterações na formação do cristalino e é a principal causa de cegueira na infância. ','<p>Qualquer opacifica&ccedil;&atilde;o do cristalino presente no nascimento &eacute; uma catarata cong&ecirc;nita. Dependendo do grau de opacifica&ccedil;&atilde;o, pode haver interfer&ecirc;ncia na passagem de luz, por distor&ccedil;&atilde;o ou redu&ccedil;&atilde;o na quantidade de raios luminosos que atingem a retina de beb&ecirc;s.</p>\r\n\r\n<p>Por ser uma causa comprovada de cegueira infantil e por requerer diagn&oacute;stico precoce e tratamento cir&uacute;rgico imediatos, a catarata cong&ecirc;nita depende de aten&ccedil;&atilde;o especial de profissionais de sa&uacute;de. O diagn&oacute;stico acurado e precoce &eacute; a chave para evitar complica&ccedil;&otilde;es irrevers&iacute;veis, e deve ser importante a participa&ccedil;&atilde;o de pediatras, obstetras e de neonatologistas para a averigua&ccedil;&atilde;o correta desse problema de sa&uacute;de visual precoce.</p>\r\n','2016-06-15 19:02:12','2016-06-15 19:02:12'),(12,12,'PRESBIOPIA','presbiopia_20160620182153.jpg','É a popular \"vista cansada\", dificuldade para ver de perto após os 40 anos.','<p>O exame oftalmol&oacute;gico determina o grau dos &oacute;culos necess&aacute;rio para o paciente. Em alguns casos &eacute;&nbsp;poss&iacute;vel uso de lentes de contato ou cirurgia.</p>\r\n','2016-06-20 21:21:54','2016-06-23 21:04:30'),(13,13,'PTERIGIO','pterigio_20160623103739.jpg','E uma membrana ( \"pele\") que pode se formar nos olhos e pode requerer cirurgia. ','<p>Pode estar relacionado a exposi&ccedil;&atilde;o solar prolongada&nbsp;e a predisposicao gen&eacute;tica. Utilizamos cola biol&oacute;gica para evitar recidivas ap&oacute;s a cirurgia.</p>\r\n','2016-06-23 13:37:39','2016-06-28 14:07:40'),(14,6,'CONJUNTIVITE','colirio-para-conjuntivite_20160623104504.jpg','Consiste na inflamação da conjuntiva e pode ter causa alérgica, bacteriana, viral ou química. \r\nO exame oftalmológico completo identifica a causa e orienta o tratamento adequado.','<p>Importante tratar o quanto antes para evitar complica&ccedil;&otilde;es e transmiss&atilde;o a outras pessoas, nos casos de conjuntivites infecciosas (bacterianas ou virais).</p>\r\n','2016-06-23 13:45:04','2016-06-28 14:04:59'),(15,7,'CORPO ESTRANHO/CISCO','irritacao_20160623105217.jpg','Causa irritação ocular e sensação de areia nos olhos. Deve ser removido o quanto antes para evitar complicações.','<p>Os corpos estranhos (ciscos) podem levar a infec&ccedil;&otilde;es e outras complica&ccedil;&otilde;es que podem prejudicar a vis&atilde;o.</p>\r\n\r\n<p>Deve-se utilizar equipamentos de prote&ccedil;&atilde;o individual sempre que poss&iacute;vel para evit&aacute;-los.</p>\r\n','2016-06-23 13:52:17','2016-06-23 21:02:26');
/*!40000 ALTER TABLE `doencas_oculares` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `exames`
--

DROP TABLE IF EXISTS `exames`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `exames` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `exames`
--

LOCK TABLES `exames` WRITE;
/*!40000 ALTER TABLE `exames` DISABLE KEYS */;
INSERT INTO `exames` VALUES (1,10,'TESTE DE LENTES DE CONTATO','Gelatinosas, rígidas e especiais para ceratocone','O objetivo das lentes de contato é proporcionar uma melhor visão no dia-a-dia. As vantagens das lentes de contato em relação aos óculos são: maior campo visual, menor distorção e tamanho mais realista dos objetos, aumento da auto-confiança e eliminação dos óculos embaçados e dos pontos de pressão no nariz e nas orelhas.','2016-06-20 19:09:11','2016-06-20 20:20:05'),(2,5,'EXAME COMPUTADORIZADO','','Exame computadorizado do grau e medida da ceratometria (curvatura da córnea)\r\n','2016-06-20 19:13:31','2016-06-20 19:13:31'),(3,12,'TONOMETRIA','','Realiza a medição da pressão intraocular, principalmente em pacientes com glaucoma ou suspeita desta doença.','2016-06-20 20:21:17','2016-06-20 20:21:17'),(4,11,'TESTE DE VISÃO DE CORES','','Utilizado para diagnóstico de deficiência na visão de cores (daltonismo) através de tabelas específicas.','2016-06-20 20:22:14','2016-06-20 21:14:19'),(5,9,'REFRAÇÃO','','É realizado para conhecer o grau dos óculos de erros refrativos como miopia, hipermetropia, astigmatismo e presbiopia.','2016-06-20 20:23:02','2016-06-23 18:02:56'),(6,8,'PAQUIMETRIA ULTRASSÔNICA','','Avalia a espessura da córnea, fundamental nos casos de pré-operatório de cirurgia refrativa a laser, edema da córnea (após cirurgias, uso de lentes de contato, distrofias corneanas, entre outras) e glaucoma.','2016-06-20 20:23:54','2016-06-21 14:16:25'),(7,7,'MAPEAMENTO DE RETINA','','É um exame complementar (não faz parte da consulta normal), no qual todo o fundo do olho e suas estruturas são avaliados.','2016-06-20 20:24:59','2016-06-21 14:16:09'),(8,4,'CERATOMETRIA','','É a medida da curvatura da superfície anterior da córnea.  Importante para adaptação de lentes de contato e para acompanhamento de casos de ceratocone.','2016-06-20 20:27:37','2016-06-20 20:27:37'),(9,2,'BIOMETRIA OCULAR','','Exame utilizado para calcular o grau da lente intraocular que será implantada na cirurgia de catarata.','2016-06-20 20:29:01','2016-06-20 21:17:11'),(10,3,'BIOMICROSCOPIA','','Tem a função de avaliar as estruturas do segmento anterior (córnea, íris, cristalino). Já a biomicroscopia de fundo analisa o fundo de olho (nervo óptico e retina central). Ambos são realizados através de um biomicroscópio, mais conhecido como lâmpada de fenda.','2016-06-20 20:30:09','2016-06-20 22:02:24'),(11,1,'ACUIDADE VISUAL','','Mede o grau de aptidão do olho para discriminar os detalhes, ou seja, a capacidade de perceber a forma e o contorno de figuras. Utilizamos tabelas específicas para crianças e para pacientes que não sabem ler.','2016-06-20 20:32:35','2016-06-21 14:14:50'),(12,6,'FUNDO DE OLHO','Fundoscopia','Consiste em examinar o nervo óptico e a retina, com suas artérias e veias, através dos meios transparentes do olho.','2016-06-20 20:37:58','2016-06-21 02:57:29'),(13,13,'TOPOGRAFIA DE CORNEA','Ceratoscopia computadorizada de cornea',' Exame que mostra, ponto a ponto, as variações da curvatura corneana, permitindo o diagnóstico de doenças da superfície da córnea, como por exemplo ceratocone e degeneração marginal pelúcida. É importante também para avaliação pré-operatória de cirurgia refrativa e adaptação de algumas lentes de contato especiais.','2016-06-20 20:41:38','2016-06-21 14:17:49');
/*!40000 ALTER TABLE `exames` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `institucional`
--

DROP TABLE IF EXISTS `institucional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `institucional` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `imagem_1` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_2` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `imagem_3` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `texto` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `institucional`
--

LOCK TABLES `institucional` WRITE;
/*!40000 ALTER TABLE `institucional` DISABLE KEYS */;
INSERT INTO `institucional` VALUES (1,'vistaclinconsultorio1_20160615134049.JPG','vistaclinconsultorio2_20160615134055.JPG','vistaclinespera_20160615134059.JPG','<p>Fundada h&aacute; mais de 10&nbsp;anos, a <strong>Vistaclin Cl&iacute;nica de Olhos&nbsp;</strong>&eacute; refer&ecirc;ncia na cidade de Carapicu&iacute;ba.Contamos com profissionais experientes e qualificados, com&nbsp;titulo de especialistas pelo Conselho Brasileiro de Oftalmologia,&nbsp;para um excelente atendimento aos pacientes<strong>.</strong></p>\r\n\r\n<p>A cl&iacute;nica possui uma infraestrutura moderna&nbsp;e&nbsp;equipamentos para realiza&ccedil;&atilde;o de exames, al&eacute;m&nbsp;de constante atualiza&ccedil;&atilde;o da equipe.<br />\r\n<br />\r\nFazemos atendimento particular e a&nbsp;conv&ecirc;nios, adultos e crian&ccedil;as, atendimentos a&nbsp;urg&ecirc;ncias&nbsp;e cirurgias.<br />\r\n<br />\r\nFazemos parcerias com empresas para realiza&ccedil;&atilde;o de exames per&iacute;odicos e admissionais.</p>\r\n',NULL,'2016-06-21 14:20:14');
/*!40000 ALTER TABLE `institucional` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `migrations`
--

DROP TABLE IF EXISTS `migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `migrations`
--

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;
INSERT INTO `migrations` VALUES ('2016_02_01_000000_create_users_table',1),('2016_03_01_000000_create_contato_table',1),('2016_03_01_000000_create_contatos_recebidos_table',1),('2016_06_07_135622_create_banners_table',1),('2016_06_07_140745_create_institucional_table',1),('2016_06_07_140910_create_corpo_clinico_table',1),('2016_06_07_142336_create_procedimentos_table',1),('2016_06_07_143932_create_convenios_table',1),('2016_06_07_161359_create_doencas_oculares_table',1);
/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `procedimentos`
--

DROP TABLE IF EXISTS `procedimentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `procedimentos` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ordem` int(11) NOT NULL DEFAULT '0',
  `titulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `subtitulo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `descricao` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `procedimentos`
--

LOCK TABLES `procedimentos` WRITE;
/*!40000 ALTER TABLE `procedimentos` DISABLE KEYS */;
INSERT INTO `procedimentos` VALUES (5,4,'APLICAÇÃO DE TOXINA BOTULÍNICA','','Para casos de blefaroespasmo ou espasmo hemifacial, que são contrações involuntárias dos músculos da face, além de indicações estéticas, como para tratamento de rugas de expressão.','2016-06-15 17:36:48','2016-06-23 20:59:44'),(15,2,'CIRURGIA DE CATARATA','','Remoção do cristalino opacificado (catarata) com implante de lente intraocular, através de microincisão. \r\n','2016-06-20 19:14:54','2016-06-20 19:14:54'),(16,3,'CIRURGIA REFRATIVA','','Realizada a laser, para correção de miopia, hipermetropia e astigmatismo.','2016-06-20 19:15:32','2016-06-20 19:15:32'),(17,1,'CIRURGIA DE PTERÍGIO','Exerese de pterigio','Retirada do pterígio com utilização de cola biológica especial para evitar recidivas (evitar que volte).\r\n','2016-06-20 19:16:26','2016-06-20 20:54:47'),(18,5,'PEQUENAS CIRURGIAS','','Realizadas na própria clínica.','2016-06-20 19:16:45','2016-06-20 22:07:47');
/*!40000 ALTER TABLE `procedimentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (1,'trupe','contato@trupe.net','$2y$10$7ZblEKiLnfMm67M9zj3ZI.N8969HbUqj.G4CY489WsgPTaFHxKz16','HNZWFWBvvte3KXPUDjRjfZB1e0f2p0SkTa55xu0it7I3TaN4fDPj0m2TYu5M',NULL,'2016-06-14 16:53:23'),(2,'vistaclin','vistaclin@vistaclin.com.br','$2y$10$uOzGPQ3SfyhDhyKnGyWUxu9vnjaTRiM4xctSc9zMvMxLtkusgRKEu','VLh1EAjTkldqXibj3DIaWnYRcGNfY3BpJyOfnW1VJPFnftd3OEC1sYilyS0P','2016-06-20 19:19:46','2016-06-20 19:43:48');
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-06-28 16:02:32
